#include <Servo.h>
#include "DHT.h"
#include <SoftwareSerial.h>

#define DHTPIN 13
#define DHTTYPE DHT22

#define RANGE 100
#define SPEED 90
#define SIDE_SPEED 90
#define LEFT_CENTER 95
#define RIGHT_CENTER 95
#define LEFT_SERVO_DIRECTION 0
#define RIGHT_SERVO_DIRECTION 1

String sInputString = ""; // Serial
boolean sStringComplete = false;
String bInputString = ""; // Bluetooth
boolean bStringComplete = false;

Servo leftServo;
Servo rightServo;
DHT dht(DHTPIN, DHTTYPE);

SoftwareSerial bluetooth(2, 3); // RX, TX

void setup() {
  Serial.begin(9600); // Debug
  sInputString.reserve(200);
  bInputString.reserve(200);
  leftServo.attach(5);
  rightServo.attach(4);

  dht.begin();

  bluetooth.begin(9600);

  pinMode(A0, OUTPUT);
  pinMode(A1, OUTPUT);
}

void loop() {

  // Serial for debugging
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      sStringComplete = true;
    }else{
      sInputString += inChar;
    }

    if (sStringComplete) {
      handleInput(sInputString);
      sInputString = "";
      sStringComplete = false;
    }
  }

  // Bluetooth
  while (bluetooth.available()) {
    char inChar = (char)bluetooth.read();
    if (inChar == '\n') {
      bStringComplete = true;
    }else{
      bInputString += inChar;
    }

    if (bStringComplete) {
      handleInput(bInputString);
      bInputString = "";
      bStringComplete = false;
    }
  }
  
}

void handleInput(String input){
  if (input[0] == 'm'){ // move command
    handleServo(input.substring(1));
  }else if (input[0] == 's'){ // sensor command
    handleSensor(input.substring(1));
  }else if (input[0] == 'l'){ // LED command
    handleLed(input.substring(1));
  }
}

void handleSensor(String input){
  float humidity = dht.readHumidity();
  float temperature = dht.readTemperature();
  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.print("%, Temprature: ");
  Serial.print(temperature);
  Serial.print(" C\n");
  bluetooth.print("Humidity: ");
  bluetooth.print(humidity);
  bluetooth.print("%, Temprature: ");
  bluetooth.print(temperature);
  bluetooth.print(" C\n");
}

void handleServo(String input){
  int index = input.indexOf(',');
  int x = input.substring(0, index).toInt();
  int y = input.substring(index+1).toInt();
  Serial.println("----data----");
  Serial.println("x: " + String(x));
  Serial.println("y: " + String(y));
  Serial.println("------------");
  updateMove(x, y);
}

void handleLed(String input){
  int index = input.indexOf(',');
  int led = input.substring(0, index).toInt();
  boolean state = input.substring(index+1).toInt();
  Serial.println("----led----");
  Serial.println("led: " + String(led));
  Serial.println("state: " + String(state));
  Serial.println("-----------");
  if (led == 0){
    digitalWrite(A0, state);
  }else if (led == 1){
    digitalWrite(A1, state);
  }
}

void updateMove(int x, int y){
  int speedX = (int)(((float)y/RANGE)*SPEED);
  int speedY = (int)(((float)x/RANGE)*SIDE_SPEED);
  
  int leftPos = LEFT_CENTER;
  if (LEFT_SERVO_DIRECTION) { leftPos += speedX; leftPos += speedY; } else { leftPos -= speedX; leftPos -= speedY; }
  int rightPos = RIGHT_CENTER;
  if (RIGHT_SERVO_DIRECTION) { rightPos += speedX; rightPos -= speedY; } else { rightPos -= speedX; rightPos += speedY; }
  
  leftServo.write(leftPos);
  rightServo.write(rightPos);
}
