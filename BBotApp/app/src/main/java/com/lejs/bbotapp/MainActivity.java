package com.lejs.bbotapp;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;;
import android.content.res.Configuration;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

// Bluetooth (https://github.com/harry1453/android-bluetooth-serial)
import com.harrysoft.androidbluetoothserial.BluetoothManager;
import com.harrysoft.androidbluetoothserial.BluetoothSerialDevice;
import com.harrysoft.androidbluetoothserial.SimpleBluetoothDeviceInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private BluetoothManager bluetoothManager;
    private SimpleBluetoothDeviceInterface deviceInterface;

    private FloatingDot dot;

    // Determines whether to send the gyro data or not
    boolean shouldMove = false;

    // Save the last time a move command was sent to limit the rate of updates not to overwhelm the bluetooth interface
    long lastSendMove;
    final int MIN_DELAY = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setup BluetoothManager
        bluetoothManager = BluetoothManager.getInstance();
        if (bluetoothManager == null) {
            // Bluetooth unavailable on this device
            Toast.makeText(this, "Bluetooth not available.", Toast.LENGTH_LONG).show();
            finish();
        }

        // Find the reference to the FloatingDot that visualises the gyro data
        dot = findViewById(R.id.dot);
        dot.setMainActivity(this);
        dot.startGyro();
    }

    public void sendMove(float x, float y){
        // Send data to robot
        if (shouldMove && deviceInterface != null) {//&& System.currentTimeMillis()-lastSendMove > MIN_DELAY) {
            // The x axis
            int xm = (int) (x * 100);
            // Keep the values between -100 and 100
            if (xm > 100) xm = 100;
            if (xm < -100) xm = -100;

            // The y axis
            int ym = (int) (y * 100);
            // Keep the values between -100 and 100
            if (ym > 100) ym = 100;
            if (ym < -100) ym = -100;

            // Send
            sendData("m" + ym + "," + xm);

            lastSendMove = System.currentTimeMillis();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // Ignore orientation change
        super.onConfigurationChanged(newConfig);
    }

    private void onConnected(BluetoothSerialDevice connectedDevice) {
        // TODO: inform user
        deviceInterface = connectedDevice.toSimpleDeviceInterface();
        deviceInterface.setListeners(this::onMessageReceived, this::onMessageSent, this::onError);
    }

    private void onMessageSent(String message){
        // A message was sent
        // Do nothing
    }

    private void onMessageReceived(String message) {
        // Display the received data in a toast
        Toast.makeText(this, "BBot says: " + message, Toast.LENGTH_LONG).show();
    }

    private void onError(Throwable error) {
        // Something went wrong
        Log.e("BLUETOOTH_ERROR", error.toString());
    }

    public void onSelectDevice(View view) {
        // Get the list of paired devices
        List<BluetoothDevice> pairedDevices = bluetoothManager.getPairedDevicesList();

        // Make a list of options if form of strings to be presented to the user
        List<String> options  = new ArrayList<>();
        for (BluetoothDevice device : pairedDevices) {
            options.add(device.getName() + ", " + device.getAddress());
        }

        // Build the dialog popup
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Select device");
        builder.setItems(options.toArray(new String[0]), (dialog, item) -> {
            // Notify the user about the selected device
            Toast.makeText(MainActivity.this, "Connecting to: " + options.get(item), Toast.LENGTH_LONG).show();
            // Call the method to connect to the selected device
            connectDevice(pairedDevices.get(item).getAddress());

        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    // Connect to a bluetooth device by it's mac address
    @SuppressLint("CheckResult")
    private void connectDevice(String mac) {
        bluetoothManager.openSerialDevice(mac)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onConnected, this::onError);
    }

    // A method to abstract the nullcheck and newline
    private void sendData(String data){
        if (deviceInterface != null){ // Check if the device is connected
            deviceInterface.sendMessage(data + "\n"); // Send the data with a closing newline
        }
    }

    // Disconnect from the bluetooth device
    public void onDisconnectDevice(View view) {
        bluetoothManager.close();
    }

    // Request an update from the sensor
    public void onReadSensor(View view) {
        sendData("s");
    }

    // Turn the first LED on
    public void onLed1On(View view) {
        sendData("l0,1");
    }

    // Turn the first LED off
    public void onLed1Off(View view) {
        sendData("l0,0");
    }

    // Turn the second LED on
    public void onLed2On(View view) {
        sendData("l1,1");
    }

    // Turn the second LED off
    public void onLed2Off(View view) {
        sendData("l1,0");
    }

    // Change the shouldMove boolean so the data from the gyro will be sent out
    public void onStartBtn(View view) {
        shouldMove = true;
    }

    // Change the shouldMove boolean so the data from the gyro will not be sent out and send a message to stop the bbot
    public void onStopBtn(View view) {
        shouldMove = false;
        dot.setPos(0,0);
        sendData("m0,0");
    }

    // Enable the gyro
    public void onGyroBtn(View view) {
        dot.setUseGyro(true);
    }

    // Use touch for control
    public void onTouchBtn(View view) {
        dot.setUseGyro(false);
        dot.setPos(0, 0);
    }
}
