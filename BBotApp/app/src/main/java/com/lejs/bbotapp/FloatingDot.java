package com.lejs.bbotapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import static android.content.Context.SENSOR_SERVICE;

public class FloatingDot extends View {

    float xPos = 0;
    float yPos = 0;
    final int radius = 30;

    Paint paint;

    final int borderWidth = 30;
    Rect border = new Rect(0, 0, 0, 0);

    float w;
    float h;

    boolean useGyro = true;

    MainActivity main;

    public FloatingDot(Context context) {
        super(context);
        setup();
    }

    public FloatingDot(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    void setup(){
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(borderWidth);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Get dimensions
        w = getWidth();
        h = getHeight();

        // Draw the border
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GREEN);
        border.bottom = getHeight();
        border.right = getWidth();
        canvas.drawRect(border, paint); // Border

        // Draw the dot
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);

        float x = (xPos +1) /2  * getWidth();
        float y = (1 -yPos) /2  * getHeight();

        canvas.drawCircle(x, y, radius, paint);

        //Log.d("FloatingDot", "x: " + xPos + ", y: " + yPos); // Only for debug purposes
    }

    public void setUseGyro(boolean b){
        useGyro = b;
    }

    public void setMainActivity(MainActivity main){
        this.main = main;
    }

    public void startGyro(){

        // Setup rotation vector sensor
        SensorManager sensorManager = (SensorManager) main.getSystemService(SENSOR_SERVICE);
        Sensor rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR); // TYPE_GAME_ROTATION_VECTOR takes into account multiple motion sensors except the geomagnetic field

        // Create a listener
        SensorEventListener rvListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (sensorEvent.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR) {
                    float[] rotationMatrix = new float[16];
                    SensorManager.getRotationMatrixFromVector(
                            rotationMatrix, sensorEvent.values);

                    // Convert to orientations
                    float[] orientations = new float[3];
                    SensorManager.getOrientation(rotationMatrix, orientations);

                    // Adjust the data to be between -1 and 1
                    float x = (float)((orientations[1]/Math.PI) * -2.0f);
                    float y = (float)((orientations[2]/Math.PI) *  2.0f);

                    if (useGyro) {
                        // Update the position of the dot
                        setPos(x, y);

                        main.sendMove(x, y);
                    }

                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
                // Do nothing
            }
        };

        // Register the listener for the gyro sensor
        sensorManager.registerListener(rvListener,
                rotationVectorSensor, SensorManager.SENSOR_DELAY_UI); // Here the delay between sensor updates is set
    }

    public void setPos(float x, float y){
        // Change the coordinates
        xPos = x;
        yPos = y;

        // Cutoff
        if (xPos > 1){ xPos = 1; }
        if (yPos > 1){ yPos = 1; }

        // Update the view
        invalidate();

        main.sendMove(x, y);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        ViewConfiguration vc = ViewConfiguration.get(this.getContext());

        setUseGyro(false);

        switch (ev.getAction()){
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                setPos((2*ev.getX()/w)-1, (-2*ev.getY()/h)+1);
                break;
            case MotionEvent.ACTION_UP:
                setPos(0, 0);
                break;
        }

        return true;
    }
}
