#include <Servo.h>
#include <SoftwareSerial.h>

#define RANGE 100
#define SPEED 90
#define SIDE_SPEED 90
#define LEFT_CENTER 89
#define RIGHT_CENTER 89
#define LEFT_SERVO_DIRECTION 1
#define RIGHT_SERVO_DIRECTION 1

#define LED1_PIN 11
#define LED2_PIN 9
#define BUZZER_PIN 13

String sInputString = ""; // Serial
boolean sStringComplete = false;
String bInputString = ""; // Bluetooth
boolean bStringComplete = false;

Servo leftServo;
Servo rightServo;

SoftwareSerial bluetooth(2, 3); // RX, TX

void setup() {
  Serial.begin(9600); // Debug
  sInputString.reserve(200);
  bInputString.reserve(200);
  leftServo.attach(5);
  rightServo.attach(4);
  updateMove(0, 0);

  bluetooth.begin(9600);

  pinMode(LED1_PIN, OUTPUT);
  pinMode(LED2_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop() {

  // Serial for debugging
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      sStringComplete = true;
    }else{
      sInputString += inChar;
    }

    if (sStringComplete) {
      handleInput(sInputString);
      sInputString = "";
      sStringComplete = false;
    }
  }

  // Bluetooth
  while (bluetooth.available()) {
    char inChar = (char)bluetooth.read();
    if (inChar == '\n') {
      bStringComplete = true;
    }else{
      bInputString += inChar;
    }

    if (bStringComplete) {
      handleInput(bInputString);
      bInputString = "";
      bStringComplete = false;
    }
  }
  
}

void handleInput(String input){
  if (input[0] == 'm'){ // move command
    handleServo(input.substring(1));
  }else if (input[0] == 's'){ // sensor command
    handleSensor(input.substring(1));
  }else if (input[0] == 'l'){ // LED command
    handleLed(input.substring(1));
  }
}

void handleSensor(String input){
  Serial.print("Humidity: none");
  Serial.print(", Temprature: none\n");
  bluetooth.print("Humidity: none");
  bluetooth.print(", Temprature: none\n");
}

void handleServo(String input){
  int index = input.indexOf(',');
  int x = input.substring(0, index).toInt();
  int y = input.substring(index+1).toInt();
  Serial.println("----data----");
  Serial.println("x: " + String(x));
  Serial.println("y: " + String(y));
  Serial.println("------------");
  updateMove(x, y);
}

void handleLed(String input){
  int index = input.indexOf(',');
  int led = input.substring(0, index).toInt();
  boolean state = input.substring(index+1).toInt();
  Serial.println("----led----");
  Serial.println("led: " + String(led));
  Serial.println("state: " + String(state));
  Serial.println("-----------");

  digitalWrite(BUZZER_PIN, HIGH);
  if (led == 0){
    digitalWrite(LED1_PIN, state);
  }else if (led == 1){
    digitalWrite(LED2_PIN, state);
  }
  delay(100);
  digitalWrite(BUZZER_PIN, LOW);
}

void updateMove(int x, int y){
  int speedX = (int)(((float)y/RANGE)*SPEED);
  int speedY = (int)(((float)x/RANGE)*SIDE_SPEED);
  
  int leftPos = LEFT_CENTER;
  if (LEFT_SERVO_DIRECTION) { leftPos += speedX; leftPos += speedY; } else { leftPos -= speedX; leftPos -= speedY; }
  int rightPos = RIGHT_CENTER;
  if (RIGHT_SERVO_DIRECTION) { rightPos += speedX; rightPos -= speedY; } else { rightPos -= speedX; rightPos += speedY; }
  
  leftServo.write(leftPos);
  rightServo.write(rightPos);
}
